package model;

import java.awt.Point;
import java.util.Random;

public class Rechteck {
	private Punkt p;
	private int breite;
	private int hoehe;
	
	public Rechteck() {
		super();
		p = new Punkt();
		setX(0);
		setY(0);
		setBreite(0);
		setHoehe(0);
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		p = new Punkt();
		setX(x);
		setY(y);
		setBreite(breite);
		setHoehe(hoehe);
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random rnd = new Random();
		Rechteck r = new Rechteck();
		
		r.setX(rnd.nextInt(1200));
		r.setY(rnd.nextInt(1000));
		r.breite = rnd.nextInt(1200 - r.getX());
		r.hoehe = rnd.nextInt(1000 - r.getY());
		return r;
	}
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(int x, int y) {
		if(x < getX() || x > getX() + getBreite())return false;
		if(y < getY() || y > getY() + getHoehe())return false;
		return true;
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		if(enthaelt(rechteck.getX(), rechteck.getY()) &&
		   enthaelt(rechteck.getX() + rechteck.breite, rechteck.getY() + rechteck.hoehe))
			return true;
		return false;
	}
	
	public int getX() {
		return p.getX();
	}
	public void setX(int x) {
		this.p.setX(x);
	}
	public int getY() {
		return p.getY();
	}
	public void setY(int y) {
		this.p.setY(y);
	}
	public int getBreite() {
		return breite;
	}
	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}
	public int getHoehe() {
		return hoehe;
	}
	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
}
