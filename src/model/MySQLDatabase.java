package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class MySQLDatabase {
	
	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";
	
	public void rechteck_eintragen(Rechteck r) {
		try {
			Connection con = getConnection();
			
			String sql = "INSERT INTO T_Rechtecke (x,y,breite,hoehe) VALUES (?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX()); //Arrays start at 1?
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			
			ps.executeUpdate();
			
			con.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Rechteck> getAllRechtecke(){
		List<Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			
			Connection con = getConnection();
			
			String sql = "SELECT * FROM T_RECHTECKE;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			
			//Pointer springt eine stelle weiter und gibt false zur�ck wenn
			//keine werte mehr vorhanden sind.
			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x, y, breite, hoehe));
			}
			
			con.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
	
	private Connection getConnection() throws SQLException, ClassNotFoundException {
		//JDBC-Treiber laden
		Class.forName(driver);
		//Verbindun aufbauen
		Connection con = DriverManager.getConnection(url, user, password);
		return con;
	}
}
