package controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.RechteckHinzufuegen;

public class BunteRechteckeController {
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		database = new MySQLDatabase();
		rechtecke = this.database.getAllRechtecke();
	}
	
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		database.rechteck_eintragen(rechteck);
	}
	
	public void reset() {
		rechtecke = new LinkedList<Rechteck>();
	}
	
	public List<Rechteck> getRechtecke(){
		return rechtecke;
	}
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + Arrays.toString(rechtecke.toArray()) + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i < anzahl; i++) {
			add(Rechteck.generiereZufallsRechteck());
		}
	}
	
	public static void main(String[] args) {
		
	}

	public void rechteckHinzufuegen() {
		// TODO Auto-generated method stub
		new RechteckHinzufuegen(this);
		
	}
	
}
