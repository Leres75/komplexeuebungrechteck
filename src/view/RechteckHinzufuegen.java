package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckHinzufuegen extends JFrame {

	private JPanel contentPane2;

	/**
	 * Launch the application.
	 */

	private BunteRechteckeController bunteRechteckeController;
	private JTextField tfXKoordinate;
	private JTextField tfYKoordinate;
	private JLabel lblBreite;
	private JTextField tfBreite;
	private JLabel lblHoehe;
	private JTextField tfHoehe;
	private JButton btnAbbrechen;
	private JButton btnHinzufuegen;
	
	/**
	 * Create the frame.
	 */
	public RechteckHinzufuegen(BunteRechteckeController bunteRechteckeController) {
		this.bunteRechteckeController = bunteRechteckeController;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane2);
		
		setLayout(new GridLayout(5, 2, 0, 0));
		
		JLabel lblXKoordinate = new JLabel("X Koordinate");
		contentPane2.add(lblXKoordinate);
		
		tfXKoordinate = new JTextField();
		contentPane2.add(tfXKoordinate);
		tfXKoordinate.setColumns(10);
		
		JLabel lblYKoordinate = new JLabel("Y Koordinate");
		contentPane2.add(lblYKoordinate);
		
		tfYKoordinate = new JTextField();
		contentPane2.add(tfYKoordinate);
		tfYKoordinate.setColumns(10);
		
		lblBreite = new JLabel("Breite");
		contentPane2.add(lblBreite);
		
		tfBreite = new JTextField();
		contentPane2.add(tfBreite);
		tfBreite.setColumns(10);
		
		lblHoehe = new JLabel("H\u00F6he");
		contentPane2.add(lblHoehe);
		
		tfHoehe = new JTextField();
		contentPane2.add(tfHoehe);
		tfHoehe.setColumns(10);
		
		btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		contentPane2.add(btnAbbrechen);
		
		btnHinzufuegen = new JButton("Hinzuf\u00FCgen");
		btnHinzufuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RechteckHinzufuegen();
				dispose();
			}
		});
		contentPane2.add(btnHinzufuegen);
		setVisible(true);
	}
	
	public void RechteckHinzufuegen() {
		Rechteck r = new Rechteck();
		r.setX(Integer.parseInt(tfXKoordinate.getText()));
		r.setY(Integer.parseInt(tfYKoordinate.getText()));
		r.setBreite(Integer.parseInt(tfBreite.getText()));
		r.setHoehe(Integer.parseInt(tfHoehe.getText()));
		bunteRechteckeController.add(r);
	}
}
