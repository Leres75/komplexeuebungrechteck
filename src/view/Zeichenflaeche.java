package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {
	private BunteRechteckeController bunteRechteckeController;

	public Zeichenflaeche(BunteRechteckeController bunteRechteckeController) {
		super();
		this.bunteRechteckeController = bunteRechteckeController;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for(Rechteck rechteck : bunteRechteckeController.getRechtecke()) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}
	}
}
