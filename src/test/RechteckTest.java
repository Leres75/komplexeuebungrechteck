package test;
import controller.BunteRechteckeController;
import model.Rechteck;
public class RechteckTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Rechteck[] rechtecke = new Rechteck[10];
		for (int i = 0; i < 5; i++) { //faulheit
			rechtecke[i] = new Rechteck(); 
		}
		
		rechtecke[0].setX(10); //ist das wirklich n�tig?...
		rechtecke[0].setY(10);
		rechtecke[0].setBreite(30);
		rechtecke[0].setHoehe(40);
		
		rechtecke[1].setX(25);
		rechtecke[1].setY(25);
		rechtecke[1].setBreite(100);
		rechtecke[1].setHoehe(20);
		
		rechtecke[2].setX(260);
		rechtecke[2].setY(10);
		rechtecke[2].setBreite(200);
		rechtecke[2].setHoehe(100);

		rechtecke[3].setX(5);
		rechtecke[3].setY(500);
		rechtecke[3].setBreite(300);
		rechtecke[3].setHoehe(25);
		
		rechtecke[4].setX(100);
		rechtecke[4].setY(100);
		rechtecke[4].setBreite(100);
		rechtecke[4].setHoehe(100);
		
		rechtecke[5] = new Rechteck(200,200,200,200);
		rechtecke[6] = new Rechteck(800,400,20,20);
		rechtecke[7] = new Rechteck(800,450,20,20);
		rechtecke[8] = new Rechteck(850,400,20,20);
		rechtecke[9] = new Rechteck(855,455,25,25);
		
		BunteRechteckeController bunteRechteckeController = new BunteRechteckeController();
		for(Rechteck rechteck : rechtecke) {
			bunteRechteckeController.add(rechteck);
		}
		System.out.println(bunteRechteckeController.toString());
		System.out.println("check if BunteRechteckeController.ToString() works: ... " + bunteRechteckeController.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"));
		//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
		    Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		    Rechteck eck11 = new Rechteck();
		    eck11.setX(-10);
		    eck11.setY(-10);
		    eck11.setBreite(-200);
		    eck11.setHoehe(-100);
		    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
		    
		Rechteck randomRechteck = Rechteck.generiereZufallsRechteck();
		System.out.println("randomRechteck: " + randomRechteck.toString());
		
		rechteckeTesten();
	}
	public static void rechteckeTesten() {
		Rechteck Canvas = new Rechteck(0, 0, 1200, 1000);
		boolean AlleInnerhalb = true;
		for(int i = 0; i<50000; i++) {
			Rechteck r = Rechteck.generiereZufallsRechteck();
			if(!Canvas.enthaelt(r)) {
				System.out.println("Au�erhalb der Grenzen: " + r.toString());
				AlleInnerhalb = false;
			}
		}
		if(AlleInnerhalb)System.out.println("Alle Innerhalb der Grenzen");
	}
}
