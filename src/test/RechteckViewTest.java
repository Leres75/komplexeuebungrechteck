package test;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.RechteckHinzufuegen;
import view.Zeichenflaeche;

@SuppressWarnings("serial")
public class RechteckViewTest extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new RechteckViewTest().run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			getContentPane().repaint();
		}

	}

	/**
	 * Create the frame.
	 */
	public RechteckViewTest() {
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.generiereZufallsRechtecke(25);
		
		setTitle("RechteckViewTest");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1260, 1080);
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JButton btnSpeichern = new JButton("Add Button");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddRechteck(brc);
			}
		});
		contentPane.add(btnSpeichern);
		
		setContentPane(contentPane);
		this.setVisible(true);
	}
	
	private void AddRechteck(BunteRechteckeController brc) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckHinzufuegen frame = new RechteckHinzufuegen(brc);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
